const express = require('express');
const app = express();
const port = process.env.PORT || 5001;

app.engine( 'html', require('ejs-locals') );
app.set( 'views', __dirname + '/../public' );
app.use(express.static(__dirname + '/../dist'))
app.get('/', (req, res) => res.render('index.html'))

app.listen(port, () => {
  console.log(`Express server listening on port ${port}`);
});
