import { normalize, schema } from 'normalizr'
import { camelizeKeys } from 'humps'

let apiUrl = 'http://localhost/v1/';
if (process.env.NODE_ENV === 'production') {
    apiUrl = 'https://api.guktong.kr/v1/';
}

const API_ROOT = apiUrl;

const callApi = (endpoint, schema) => {
  const fullUrl = (endpoint.indexOf(API_ROOT) === -1) ? API_ROOT + endpoint : endpoint

  return fetch(fullUrl)
    .then(response =>
      response.json().then(json => {
        if (!response.ok) {
          return Promise.reject(json);
        }
        return Object.assign({},
          json
        );
      })
    );
};

/*--- User ---*/
// Define User Answer Schema
const userAnswer = new schema.Entity('user_answers')

// Define User Survey Schema
const userSurvey = new schema.Entity('user_surveys', {
  answers: [ userAnswer ]
});

// Define User Petition Schema
const userPetition = new schema.Entity('user_petitions');

// Define User Schema
const userSchema = new schema.Entity('users', {
  surveys: [ userSurvey ],
  petitions: [ userPetition ]
});




/*--- Petition ---*/
// Define Petition Reference Link Schema
const link = new schema.Entity('links');

// Define Petition Schema
const petitionSchema = new schema.Entity('petitions', {
  references: [ link ]
})



/*--- Survey ---*/
// Define Anwser Schema
const answer = new schema.Entity('answers');

// Define Question Schema
const question = new schema.Entity('questions', {
  answers: [ answer ]
});

// Define Survey Schema
const surveySchema = new schema.Entity('surveys', {
  questions: [question]
});



// Schemas for Github API responses.
export const Schemas = {
  USER: userSchema,
  USER_ARRAY: [userSchema],
  PETITION: petitionSchema,
  PETITION_ARRAY: [petitionSchema],
  SURVEY: surveySchema,
  SURVEY_ARRAY: [surveySchema]
}




// Action key that carries API call info interpreted by this Redux middleware.
export const CALL_API = 'Call API'

// A Redux middleware that interprets actions with CALL_API info specified.
// Performs the call and promises when such actions are dispatched.
export default store => next => action => {
  const callAPI = action[CALL_API]
  if (typeof callAPI === 'undefined') {
    return next(action)
  }

  let { endpoint } = callAPI
  const { schema, types } = callAPI

  if (typeof endpoint === 'function') {
    endpoint = endpoint(store.getState())
  }

  if (typeof endpoint !== 'string') {
    throw new Error('Specify a string endpoint URL.')
  }
  if (!schema) {
    throw new Error('Specify one of the exported Schemas.')
  }
  if (!Array.isArray(types) || types.length !== 3) {
    throw new Error('Expected an array of three action types.')
  }
  if (!types.every(type => typeof type === 'string')) {
    throw new Error('Expected action types to be strings.')
  }

  const actionWith = data => {
    const finalAction = Object.assign({}, action, data)
    delete finalAction[CALL_API]
    return finalAction
  }

  const [ requestType, successType, failureType ] = types
  next(actionWith({ type: requestType }))

  return callApi(endpoint, schema).then(
    response => next(actionWith({
      response,
      type: successType
    })),
    error => next(actionWith({
      type: failureType,
      error: error.message || 'Something bad happened'
    }))
  )
}