import 'whatwg-fetch';
// import _ from 'lodash';
import Cookies from 'js-cookie';
import {
    DATA_UPDATED,
    RECENT_MESSAGES_RECEIVED,
    CONGRESSMAN_STATUS_RECEIVED,
    RECEIVED_LAWMAKERS,
    RESPONDS_RECEIVED
    // SAVE_FORM_DATA,
    // LOAD_FORM_DATA
} from './constants';

let apiUrl = 'http://localhost:3333/v1';

if (process.env.NODE_ENV === 'production') {
    apiUrl = 'https://api.sotong.co/v1';
} else if (process.env.NODE_ENV === 'staging') {
    apiUrl = 'https://dev-api.sotong.co/v1';
}

function request(opts) {
    const { url, method = 'GET', body, access_token } = opts;
    const fetch_param = {
        credentials: 'include',
        method,
        headers: {
            'Content-Type': 'text/plain',
            Accept: 'text/plain'
        }
    };
    if (access_token) {
        fetch_param.headers.Authorization = 'Bearer ' + access_token;
    }

    if (body) {
        fetch_param.body = JSON.stringify(body);
    }

    return fetch(url, fetch_param).then(resp => {
        if (resp.status !== 200) {
            let message = `${url} response ${resp.status}`;
            if (body) message += `: ${JSON.stringify(body)}`;
            throw message;
        } else {
            return resp.json();
        }
    });
}

function handleException(err) {
    if (process.env.NODE_ENV === 'production') {
        window.alert(
            '죄송합니다! 서버에 문제가 생겼습니다.\n' +
                '새로고침 하여 다시 시도해보시겠어요?'
        );
    } else {
        console.error(err);
    }
    return err;
}

function facebookLogin(fb_auth_code) {
    const referrer = Cookies.get('referrer') || document.referrer;
    return (dispatch, getState) => {
        return request({
            url: apiUrl + '/auth/facebook',
            method: 'POST',
            access_token: fb_auth_code,
            body: {
                referrer: referrer
            }
        })
            .then(json => {
                if (json.access_token && json.user) {
                    const payload = {};
                    payload.access_token = json.access_token;
                    payload.user = json.user;
                    payload.show_social_login = false;

                    Cookies.set('access_token', json.access_token);
                    return Promise.resolve(
                        dispatch({
                            type: DATA_UPDATED,
                            result: Object.assign({}, getState().data, payload)
                        })
                    );
                }
                return handleException({
                    message: 'facebook login err'
                });
            })
            .catch(err => {
                return handleException(err);
            });
    };
}

function naverLogin(naver_auth_code, naver_auth_state) {
    return (dispatch, getState) => {
        return request({
            url: apiUrl + '/auth/naver',
            method: 'POST',
            body: {
                code: naver_auth_code,
                state: naver_auth_state
            }
        })
            .then(json => {
                if (json.access_token && json.user) {
                    const payload = {};
                    payload.access_token = json.access_token;
                    payload.user = json.user;
                    payload.show_social_login = false;

                    Cookies.set('access_token', json.access_token);
                    return Promise.resolve(
                        dispatch({
                            type: DATA_UPDATED,
                            result: Object.assign({}, getState().data, payload)
                        })
                    );
                }

                return handleException({
                    message: 'naver login err'
                });
            })
            .catch(err => {
                handleException(err);
            });
    };
}

function fetchUserDataWithToken(access_token) {
    const url = apiUrl + '/auth/token';
    return (dispatch, getState) => {
        return request({
            method: 'POST',
            url,
            access_token
        })
            .then(json => {
                if (json.access_token && json.user) {
                    Cookies.set('access_token', json.access_token, {
                        expires: 7
                    });
                    const payload = {};
                    payload.access_token = json.access_token;
                    payload.user = json.user;
                    payload.show_social_login = false;

                    return Promise.resolve(
                        dispatch({
                            type: DATA_UPDATED,
                            result: Object.assign({}, getState().data, payload)
                        })
                    );
                }
                return Cookies.set('access_token', '');
            })
            .catch(() => {
                return Cookies.set('access_token', '');
            });
    };
}

function fetchData() {
    return (dispatch, getState) => {
        const currState = getState();
        if (currState.data.data) {
            return Promise.resolve(
                dispatch({
                    type: DATA_UPDATED,
                    result: Object.assign({}, currState.data.data)
                })
            );
        }
        const url = apiUrl + '/petitions/ongoing';
        return request({ url })
            .then(json => {
                const data = json[0];
                return Promise.resolve(
                    dispatch({
                        type: DATA_UPDATED,
                        result: Object.assign({}, currState.data, {data})
                    })
                );
            })
            .catch(err => {
                handleException(err);
            });
    };
}

function fetchRecentMessages() {
    return (dispatch, getState) => {
        const currState = getState();
        const petition_id = currState.data.data.id;
        return request({
            url: apiUrl + `/petitions/${petition_id}/messages`,
            limit: 10
        })
            .then(json => {
                return Promise.resolve(
                    dispatch({
                        type: RECENT_MESSAGES_RECEIVED,
                        result: json
                    })
                );
            })
            .catch(err => {
                handleException(err);
            });
    };
}

/**
 *
 * @param {object} opts {congressman_id, message}
 */
function sendUserPetition(opts) {
    return (dispatch, getState) => {
        let currState = getState();
        const user_id = currState.data.user.id;
        const petition_id = currState.data.data.id;
        return request({
            url: apiUrl + `/users/${user_id}/petitions/${petition_id}`,
            method: 'POST',
            access_token: currState.data.access_token,
            body: opts
        })
            .then((json) => {
                currState = getState();
                let user = json.user;
                const data = Object.assign({}, currState.data);
                user = Object.assign({}, data.user, user);
                user.user_petitions = json.user_petitions;
                data.user = user;
                data.tmp_form_data = null;
                Cookies.set('tmp_form_data', '');
                return Promise.resolve(
                    dispatch({
                        type: DATA_UPDATED,
                        result: Object.assign({}, data)
                    })
                );
            })
            .catch(err => {
                handleException(err);
            });
    };
}

function fetchCongressmanStatus() {
    return (dispatch, getState) => {
        const currState = getState();
        const petition_id = currState.data.data.id;
        return request({
            url: apiUrl + `/congressmans/petitions/${petition_id}/status/all`,
        })
            .then(json => {
                return Promise.resolve(
                    dispatch({
                        type: CONGRESSMAN_STATUS_RECEIVED,
                        result: json
                    })
                );
            })
            .catch(err => {
                handleException(err);
            });
    };
}

function fetchResponds() {
    return (dispatch, getState) => {
        const currState = getState();
        const petition_id = currState.data.data.id;
        return request({
            url: apiUrl + `/petitions/${petition_id}/responds?filter=message&limit=15`,
        })
            .then(json => {
                return Promise.resolve(
                    dispatch({
                        type: RESPONDS_RECEIVED,
                        result: json
                    })
                );
            })
            .catch(err => {
                handleException(err);
            });
    };
}

function saveTempFormData(data) {
    Cookies.set('tmp_form_data', JSON.stringify(data));
    const tmp_form_data = data;
    return (dispatch, getState) => {
        const currState = getState();
        return Promise.resolve(
            dispatch({
                type: DATA_UPDATED,
                result: Object.assign({}, currState.data, {tmp_form_data})
            })
        );
    };
}

function loadTempFormData() {
    let data = Cookies.get('tmp_form_data');
    data = data ? JSON.parse(data) : null;
    return (dispatch, getState) => {
        const currState = getState();
        currState.data.tmp_form_data = data;
        return Promise.resolve(
            dispatch({
                type: DATA_UPDATED,
                result: currState.data
            })
        );
    };
}

function showSocialLogin(show = true) {
    const show_social_login = show;
    return (dispatch, getState) => {
        return Promise.resolve(
            dispatch({
                type: DATA_UPDATED,
                result: Object.assign({}, getState(), {show_social_login})
            })
        );
    };
}

function fetchLawmakers() {
    return (dispatch, getState) => {
        const currState = getState();
        if (currState.lawmakers) {
            return dispatch({
                type: RECEIVED_LAWMAKERS,
                result: currState.lawmakers
            });
        }
        return fetch('/lawmakers.json')
                .then(res => res.json())
                .then(json => {
                    return Promise.resolve(
                        dispatch({
                            type: RECEIVED_LAWMAKERS,
                            result: json
                        })
                    );
                });
    };
}

export {
    facebookLogin,
    naverLogin,
    fetchUserDataWithToken,
    fetchData,
    sendUserPetition,
    fetchCongressmanStatus,
    fetchRecentMessages,
    saveTempFormData,
    loadTempFormData,
    showSocialLogin,
    fetchLawmakers,
    fetchResponds
};
