import createHistory from 'history/createBrowserHistory';
import { applyMiddleware, createStore, compose, combineReducers } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import * as rootReducer from '../reducers';
import DevTools from '../containers/DevTools';
import thunk from 'redux-thunk';

export const history = createHistory();
const middleware = routerMiddleware(history);

export function configureStore(initialState) {
    return createStore(
        combineReducers(rootReducer),
        initialState,
        compose(
            applyMiddleware(middleware),
            applyMiddleware(thunk),
            DevTools.instrument()
        )
    );
}
