import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { section, inputBox, resultBox } from '../styles/findCongress.scss';
import innerHeight from 'ios-inner-height';
import MaterialIcon from 'material-icons-react';
import { ReactHeight } from 'react-height';
import 'whatwg-fetch';

class FindCongress extends React.Component {

    state = {
        keyword: '',
        result: [],
        lawmakersIndex: []
    }

    componentDidMount() {
        fetch('/lawmakers_index.json').then(res => res.json()).then(json => {
            const lawmakersIndex = json;
            const nextState = { lawmakersIndex };
            this.setState(nextState);
        });
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.active) {
            this.setState({
                result: [],
                keyword: ''
            });
        }
    }

    handleChange(e) {
        const { lawmakersIndex } = this.state;
        const value = e.target.value;
        const re = new RegExp(_.escapeRegExp(value), 'i');
        const isMatch = (result) => (re.test(result.title) || re.test(result.description));
        const sliced = _.filter(lawmakersIndex, isMatch).slice(0, 10);
        this.setState({
            keyword: value,
            result: value.length > 0 ? sliced : []
        });
    }

    handleSelect(item) {
        this.props.onSelect(item);
    }

    getResultEl(data) {
        const r = [];
        for (const item of data) {
            r.push(
                <li key={`c--result--${item.id}`} onClick={this.handleSelect.bind(this, item)}>
                    <h4>{item.title}</h4>
                    <p>{item.description.replace(/\,/g, ', ')}</p>
                </li>
            );
        }
        return r;
    }

    onHeightReady(height) {
        this.props.onHeightChange(height);
        this.setState({ height: height });
    }

    render() {
        const { result, keyword } = this.state;
        return (
            <ReactHeight onHeightReady={this.onHeightReady.bind(this)} className={section} style={{ minHeight: innerHeight() }}>
                <h1>의원찾기</h1>
                <div className={inputBox}>
                    <input value={keyword} onChange={this.handleChange.bind(this)} type="text" placeholder="이름 또는 지역구, 행정동으로 검색하세요. ex)강길부, 개봉동" />
                    <MaterialIcon icon="search" color="#ddd" />
                </div>
                <div className={resultBox}>
                    <ul>
                        {this.getResultEl(result)}
                    </ul>
                </div>
                {/* <button onClick={this.props.onBack}>뒤로가기</button> */}
            </ReactHeight>
        );
    }
}

FindCongress.propTypes = {
    onSelect: PropTypes.func.isRequired,
    onBack: PropTypes.func.isRequired,
    onHeightChange: PropTypes.func.isRequired,
    active: PropTypes.bool.isRequired
};

export default FindCongress;
