import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { section } from '../styles/begin.scss';
import { ReactHeight } from 'react-height';
import moment from 'moment';

class About extends React.Component {

    state = {
        data: null,
        lawmakers: null
    }

    componentWillReceiveProps(nextProps) {
        this.setState(nextProps);
        const newState = {};
        if (nextProps.data) newState.data = nextProps.data;
        if (nextProps.lawmakers) newState.lawmakers = nextProps.lawmakers;
        this.setState(newState);
    }

    onHeightReady(height) {
        this.props.onHeightChange(height);
        this.setState({height: height});
    }

    render() {
        const { data, lawmakers } = this.state;
        return (
            <ReactHeight onHeightReady={this.onHeightReady.bind(this)} className={section}>
                <div>
                    <img src="https://www.assembly.go.kr/resources/web/assm/images/img/img_assemblyCi_02.gif"/>
                    <h4><span><u>정부 개헌안</u>으로의</span> 개헌을 위한</h4>
                    <h1>"국민투표법 개정을 촉구합니다"</h1>
                    {function renderContent() {
                        if (!data) {
                            return (
                                <div><br/><br/><br/></div>
                            );
                        } else if (data && data.user && lawmakers) {
                            const user_petition = data.user.user_petitions[0];
                            if (user_petition) {
                                const date_sent = moment.utc(user_petition.created).format('YYYY년 MM월 DD일 H시 mm분');
                                const congressman = lawmakers[user_petition.congressman_id];
                                return (
                                    <div>
                                        <h3>청원 발송 완료</h3>
                                        <div>{date_sent}에 {congressman.name}의원에게 청원을 보냈습니다.</div>
                                    </div>
                                );
                            }
                        }
                        return (
                            <div>
                                <button onClick={()=>{
                                    this.props.onNext();
                                }}>의원찾기・청원하기</button>
                                <p>소통을 원하는 국회의원을 찾아 <span style={{display: 'inline-block'}}>개헌에 대한 여러분의 의견을 전달하세요.</span></p>
                            </div>
                        );
                    }.bind(this)()}
                </div>
            </ReactHeight>
        );
    }
}

About.propTypes = {
    onNext: PropTypes.func.isRequired,
    onHeightChange: PropTypes.func.isRequired,
    data: PropTypes.object,
    lawmakers: PropTypes.object
};

export default connect(state => ({
    data: state.data,
    lawmakers: state.lawmakers
}))(About);
