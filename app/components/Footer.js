import React from 'react';
import { footer, guktong, pghack, top, bottom } from '../styles/footer.scss';

class Footer extends React.Component {
    componentDidMount() {
        // Twitter
        this.initTwitterBtn();// eslint-disable-next-line no-use-before-define
    }

    initTwitterBtn() {
        !(function initTw(d, s, id) {
            let js;
            const fjs = d.getElementsByTagName(s)[0];
            const p = /^http:/.test(d.location) ? 'http' : 'https';
            if (!d.getElementById(id)) {
                js = d.createElement(s);
                js.id = id;
                js.src = p + '://platform.twitter.com/widgets.js';
                fjs.parentNode.insertBefore(js, fjs);
            }
        })(document, 'script', 'twitter-wjs');
    }

    render() {
        return (
            <footer className={footer}>
                <div className={top}>
                    <h1>
                        <span className={guktong}>국회소통</span>
                        <span className={pghack}>박근핵닷컴</span>
                    </h1>
                </div>
                <div className={bottom}>
                    <div
                        className="fb-share-button"
                        data-href="https://sotong.co"
                        data-layout="button_count"
                        data-size="small"
                        data-mobile-iframe="true"
                    />
                     <a
                        href="https://twitter.com/share"
                        className="twitter-share-button"
                        data-url="https://sotong.co"
                        data-via="sotong_co"
                        data-lang="ko"
                        data-hashtags="국회소통"
                    >
                    </a>
                    <p>
                        <a target="_blank" href="https://docs.google.com/document/d/e/2PACX-1vQvS33_aCKyrjhg53um5QMsOyB1jIqVMlV_0Xyxig0cKFAasToawXz70_Y6RD-2vxUkoTEpvOfVmm8G/pub">이용약관</a> |{' '}
                        <a target="_blank" href="https://docs.google.com/document/d/e/2PACX-1vQm0kYbm9h_y0V4mnPG2JcmM7xX8T7HknDe8J1Or8_JMJKZcOnkmD8O8FDGbM2jBLhGTy096FYbnNsW/pub">개인정보보호정책</a>
                    </p>

                    <p>hello@sotong.co</p>
                    <ul>
                        <li>
                            공지 2018.04.26
                            <br/>명확한 메세지 전달을 위해 의원에게 보내어지는 질문의 문구가 변경되었습니다.
                            <br/>기존: '개헌을 위한 국민투표법의 신속한 처리에 찬성하시나요?' ->
                            <br/>변경: '정부개헌안과 이를 위한 국민투표법의 신속한 처리에 찬성하시나요?'
                        </li>
                    </ul>
                </div>
            </footer>
        );
    }
}

export default Footer;
