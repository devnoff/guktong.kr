import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    section,
    recent,
    card,
    all,
    party,
    header,
    body,
    labelDesc,
    reactModalPortal,
    modalButton,
    contentBody,
    lazyFadeIn,
    detailBox,
    respondBody
    // vertical
} from '../styles/congressStatus.scss';
import innerHeight from 'ios-inner-height';
import Carousel from 'nuka-carousel';
import MdChevronLeft from 'react-icons/lib/md/chevron-left';
import MdChevronRight from 'react-icons/lib/md/chevron-right';
import Circle from 'react-icons/lib/md/lens';
import Blocked from 'react-icons/lib/md/cancel';
import Checked from 'react-icons/lib/md/check-circle';
import Empty from 'react-icons/lib/md/panorama-fish-eye';
import MaterialIcon from 'material-icons-react';
import FaTwitterSquare from 'react-icons/lib/fa/twitter-square';
import FaFacebookSquare from 'react-icons/lib/fa/facebook-square';
import FaGlobe from 'react-icons/lib/fa/globe';
import Modal from 'react-modal';
import {fetchCongressmanStatus, fetchResponds} from '../actions';
import _ from 'lodash';
import moment from 'moment';
moment.locale('ko', {
    relativeTime: {
        future: '이후 %s',
        past: '%s전',
        s: '1초',
        m: '1분',
        mm: '%d분',
        h: '한 시간',
        hh: '%d시간',
        d: '하루',
        dd: '%d일',
        M: '개월',
        MM: '%d개월',
        y: '년',
        yy: '%d년'
    }
});

class CongressStatus extends React.Component {

    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        lawmakers: PropTypes.object,
        status: PropTypes.array,
        data: PropTypes.object
    };

    state = {
        respondsModalData: null,
        congressmanModalData: null
    };

    componentWillReceiveProps(nextProps) {
        const { data, lawmakers, status, responds } = nextProps;
        this.setState({
            lawmakers,
            status,
            responds,
            data
        });

        if (data && !this.state.status) {
            this.props.dispatch(fetchCongressmanStatus());
        }

        if (data && lawmakers && !this.state.responds) {
            this.props.dispatch(fetchResponds());
        }
    }

    handleClickRespondCard(data) {
        this.setState({
            respondsModalData: data,
            congressmanModalData: null
        });
    }

    handleClickCongressman(data) {
        const { lawmakers } = this.state;
        const d =  Object.assign({}, data, lawmakers[data.id]);
        this.setState({
            congressmanModalData: d,
            respondsModalData: null
        });
    }

    getStatusItemEl(data, key) {
        const status = data.status;
        let color = '#bbb';
        // 답변 번호 1: 네, 찬성합니다 2: 아니요, 반대합니다
        if (status.petition_answer_id === '1') {
            color = 'rgb(62, 151, 211)';
        } else if (status.petition_answer_id === '2') {
            color = 'rgb(221, 63, 71)';
        }

        let style = { color };
        let n = data.name;
        if (n.length === 2) {
            n = <span>{n[0]}<span style={{width: '12px', display: 'inline-block'}}></span>{n[1]}</span>;
        }

        let dot = <Empty color={color}/>;
        if (['1', '2'].indexOf(status.petition_answer_id) > -1) {
            dot = <Circle color={color}/>;
        } else if (status.date_verified) {
            dot = <Checked color={color}/>;
        } else if (status.petition_cnt > 0) {
            dot = <Circle color={color}/>;
        }

        return (
            <li key={key} onClick={this.handleClickCongressman.bind(this, data)}>
                {dot}
                <span style={style}>{n}</span>
            </li>
        );
    }

    getCarouselEl() {
        const { status, responds } = this.state;

        if (!responds || responds.length < 1) {
            return undefined;
        }

        return (
            <Carousel
                className={recent}
                style={{
                    height: '152px'
                }}
                slideWidth="150px"
                renderBottomCenterControls={() => (
                    undefined
                )}
                renderCenterLeftControls={({ previousSlide }) => (
                    <button onClick={previousSlide}><MdChevronLeft size={30}/></button>
                )}
                renderCenterRightControls={({ nextSlide }) => (
                    <button onClick={nextSlide}><MdChevronRight size={30} /></button>
                )}
            >
            {responds.map(item =>
                <div className={card} key={'res-carousel-' + item.id}>
                    <div onClick={this.handleClickRespondCard.bind(this, item)}>
                        <img src={`https://parkgeunhack.com/img/${item.congressman.id}.jpg`} />
                        <h4>{item.congressman.name}</h4>
                        <p style={{color: item.petition_answer.id === '2' ? '#da8484' : 'rgb(121, 163, 226)'}}>{item.petition_answer.title}</p>
                        <a>답장보기</a>
                        <small>{moment.utc(item.date_respond).fromNow()}</small>
                        <i style={{
                            background: `url(${_.find(status, {id: item.congressman.party_id}).logo})`
                        }}></i>
                    </div>
                </div>)}
            </Carousel>
        );
    }

    getStatusEl() {
        const { status } = this.state;
        return status.map(item =>
            <div className={party} key={`party-${item.id}`}>
                <div className={header}>
                    {item.logo
                        ? <img style={{width: '60px', maxHeight: '30px', objectFit: 'contain'}} src={item.logo} />
                        : <span>{item.name}</span>}
                </div>
                <div className={body}>
                    <ul>
                        {function renderItem() {
                            const els = [];
                            for(let i = 0; i < item.congressmans.length; i++) {
                                const data = item.congressmans[i];
                                const key = `party-${item.id}-item-${i}`;
                                els.push(this.getStatusItemEl(data, key));
                            }
                            return els;
                        }.bind(this)()}
                    </ul>
                    {/* <div className={[labelDesc, vertical].join(' ')}>
                        <span><Circle color={'rgb(62, 151, 211)'}/> 성실히 임할것 : 80명</span>
                        <span><Circle color={'rgb(221, 63, 71)'}/> 차후에 처리 : 30명</span>
                        <span><Checked color={'#bbb'}/> 메일을 확인 : 80명</span>
                        <span><Circle color={'#bbb'}/> 메일 발송 : 40명</span>
                        <span><Empty color={'#bbb'}/> 받은 청원 없음 : 2명</span>
                    </div> */}
                </div>
            </div>);
    }

    getCongressmanCardEl(cman) {
        const { status } = this.state;
        if (!cman) return undefined;
        const p = _.find(status, {id: cman.party.id + ''});

        let s = '아직 응답하지 않았습니다';
        let s_color = '#ddd';
        if (cman.status.petition_answer_id) {
            s = cman.status.answer;
            s_color = cman.petition_answer_id === '2' ? '#da8484' : 'rgb(121, 163, 226)';
        }
        return (
            <div className={[card].join(' ')}>
                <div>
                    <img src={`https://parkgeunhack.com/img/${cman.id}.jpg`} />
                    <h4>{cman.name}</h4>
                    <a>{cman.district}</a>
                    <p style={{color: s_color}}>{s}</p>
                    <small><strong>{cman.status.petition_cnt}</strong>개의 청원 받음</small>
                    <i style={{
                        background: `url(${p.logo})`
                    }}>{p.id === '8' ? '무소속' : ''}</i>
                </div>
                <div className={[detailBox].join(' ')}>
                    <div>
                        <div>
                            {cman.website ? (
                                <div>
                                    <a target="blank_" href={cman.website}>
                                        <FaGlobe />
                                        <span>홈페이지</span>
                                    </a>
                                </div>
                            ) : (
                                undefined
                            )}
                            {cman.twitter ? (
                                <div>
                                    <a target="blank_" href={cman.twitter}>
                                        <FaTwitterSquare />
                                        <span>트위터</span>
                                    </a>
                                </div>
                            ) : (
                                undefined
                            )}
                            {cman.facebook ? (
                                <div>
                                    <a target="blank_" href={cman.facebook}>
                                        <FaFacebookSquare />
                                        <span>페이스북</span>
                                    </a>
                                </div>
                            ) : (
                                undefined
                            )}
                        </div>
                        <div>
                            {cman.phone ? (
                                <div>
                                    <MaterialIcon icon="local_phone" />
                                    {cman.phone}
                                </div>
                            ) : (
                                undefined
                            )}
                            {cman.fax ? (
                                <div>
                                    <MaterialIcon icon="local_printshop" />
                                    {cman.fax}
                                </div>
                            ) : (
                                undefined
                            )}
                            {cman.office ? (
                                <div>
                                    <MaterialIcon icon="domain" />
                                    {cman.office}
                                </div>
                            ) : (
                                undefined
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    getRespondCardEl(data) {
        const { status, lawmakers } = this.state;
        if (!data) return undefined;
        const p = _.find(status, {id: data.congressman.party_id});
        const cman = lawmakers[data.congressman.id];
        return (
            <div className={respondBody}>
                <div>
                    <img src={`https://parkgeunhack.com/img/${data.congressman.id}.jpg`} />
                    <h4>
                        <div>
                            <span>{data.congressman.name}</span>
                            <small>{cman.district}</small>
                        </div>
                    </h4>
                    <i style={{
                        background: `url(${p.logo})`
                    }}>{p.id === '8' ? '무소속' : ''}</i>
                </div>
                {data.screenshot_url ? <div><img style={{maxWidth: '100%'}} src={data.screenshot_url} /></div> : undefined}
                <div>
                    <p>{data.message}</p>
                    <small>{moment.utc(data.date_respond).format('YYYY.MM.DD HH시 mm분')}에 받음</small>
                </div>
            </div>
        );
    }

    getModalEl(open, contentEl, closeCallback, extraStyle) {
        return (
            <Modal
                ariaHideApp={false}
                isOpen={open}
                portalClassName={reactModalPortal}
                style={{
                    overlay: {
                        top: '0',
                        left: '0',
                        right: '0',
                        bottom: '0',
                        backgroundColor: 'rgba(0,0,0,0)',
                        overflowY: 'scroll'
                    },
                    content: {
                        top: '0',
                        left: '0',
                        right: '0',
                        bottom: '0',
                        position: 'relative',
                        border: 'none',
                        background: 'none',
                        textAlign: 'center'
                    }
                }}
            >
                <div style={extraStyle} className={[contentBody, lazyFadeIn].join(' ')}>
                    {contentEl}
                    <div className={modalButton}>
                        <button
                            style={{ verticalAlign: 'middle' }}
                            onClick={closeCallback}
                        >
                            <Blocked size={30}/>
                        </button>
                    </div>
                </div>
            </Modal>
        );
    }

    render() {
        const { data, status, responds, respondsModalData, congressmanModalData } = this.state;
        return (
            <div className={section} style={{minHeight: innerHeight()}}>
                <h1>응답 현황</h1>

                <h2 style={{
                    color: '#333',
                    margin: '3rem 0',
                    fontSize: '1.3rem',
                    border: '1px solid #eee',
                    boxShadow: '0 0 3px #eee',
                    wordBreak: 'keep-all',
                    padding: '1rem'}}>
                    <small style={{display: 'inline-block', fontSize: '.9rem', color: '#ccc', fontWeight: 'normal', paddingBottom: '1.5rem'}}>의원님들께 여쭈었습니다.</small><br/>
                    "{data && data.data.question ? data.data.question : ''}"
                    </h2>

                {responds && responds.length > 0 ? <h3>답장을 보내주신 의원님</h3> : undefined}
                {this.getCarouselEl()}
                <h3>전체 응답</h3>
                <div className={labelDesc}>
                    <span><Circle color={'rgb(62, 151, 211)'}/> 찬성합니다</span>
                    <span><Circle color={'rgb(221, 63, 71)'}/> 반대합니다</span>
                    <div style={{display: 'inline-block'}}>
                        <span><Checked color={'#bbb'}/> 메일 확인함</span>
                        <span><Circle color={'#bbb'}/> 메일 받음</span>
                        <span><Empty color={'#bbb'}/> 받은 청원 없음</span>
                    </div>
                </div>
                <div className={all}>
                    {status ? (
                        this.getStatusEl()
                    ) : (
                        <div>로딩중..</div>
                    )}
                </div>
                {this.getModalEl(respondsModalData ? true : false, this.getRespondCardEl(respondsModalData), () => {
                    this.setState({
                        respondsModalData: null
                    });
                }, { maxWidth: '400px'})}
                {this.getModalEl(congressmanModalData ? true : false, this.getCongressmanCardEl(congressmanModalData), () => {
                    this.setState({
                        congressmanModalData: null
                    });
                })}
            </div>
        );
    }
}

export default connect(state => ({
    lawmakers: state.lawmakers,
    responds: state.responds,
    status: state.status,
    data: state.data
}))(CongressStatus);
