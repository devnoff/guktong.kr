import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { section } from '../styles/socialLogin.scss';
import { ReactHeight } from 'react-height';
import { push } from 'react-router-redux';
import FaFacebookSquare from 'react-icons/lib/fa/facebook-square';

class SocialLogin extends React.Component {
    static propTypes = {
        onHeightChange: PropTypes.func.isRequired,
        dispatch: PropTypes.func.isRequired,
        data: PropTypes.object
    };

    onHeightReady(height) {
        this.props.onHeightChange(height);
        this.setState({height: height});
    }

    handleClick(uri) {
        this.props.dispatch(push(uri));
    }

    render() {
        return (
            <ReactHeight onHeightReady={this.onHeightReady.bind(this)} className={section}>
                <div>
                    <h3>SNS 간편로그인</h3>
                    <div>
                        <ul>
                            <li>SNS로 로그인하여 간편하게 서비스를 이용하실 수 있습니다</li>
                            <li>사용하시는 서비스를 선택하여 로그인 해주세요.</li>
                        </ul>
                    </div>
                </div>
                <div>
                    <div>
                        <button onClick={this.handleClick.bind(this, '/entry?login=facebook')}>
                        <FaFacebookSquare color="rgba(255,255,255,0.9)" size={18}/>
                            페이스북으로 로그인
                        </button>
                        <button onClick={this.handleClick.bind(this, '/entry?login=naver')}>
                        <span style={{width: '16px',
                                      height: '16px',
                                      display: 'flex',
                                      backgroundColor: 'rgba(255,255,255,0.9)',
                                      justifyItems: 'center',
                                      alignItems: 'center',
                                      justifyContent: 'center',
                                      borderRadius: '2px',
                                      marginRight: '5px',
                                      marginLeft: '3px',
                                      }}>
                            <img style={{ width: '10px', height: '10px'}} src={require('../src/i/Naver_logo_initial_white.svg')}/>
                        </span>
                        네이버로 로그인
                        </button>
                    </div>
                </div>
            </ReactHeight>
        );
    }
}

export default connect(state => ({
    data: state.data
}))(SocialLogin);
