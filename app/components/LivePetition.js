import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    section,
    bottomDeco,
    bounce1,
    bounce2,
    bounce3
} from '../styles/livePetition.scss';
import innerHeight from 'ios-inner-height';
import { fetchRecentMessages } from '../actions';
import _ from 'lodash';
import FlipMove from 'react-flip-move';
import CountUp from 'react-countup';
import moment from 'moment';
moment.locale('ko', {
    relativeTime: {
        future: '이후 %s',
        past: '%s전',
        s: '1초',
        m: '1분',
        mm: '%d분',
        h: '한 시간',
        hh: '%d시간',
        d: '하루',
        dd: '%d일',
        M: '개월',
        MM: '%d개월',
        y: '년',
        yy: '%d년'
    }
});

const randomFace = (man) => {
    const t = man !== undefined ? 12 : 24;
    let i = Math.floor(Math.random() * t) + 1;
    if (man) {
        i = i + t;
    }
    const name = i < 10 ? '0' + i : i;
    return require(`../src/i/face/${name}.png`);
};

const FACE_CNT = 39;

function randomGroup() {
    const r = [];
    for (let i = 0; i < FACE_CNT; i++) {
        r.push(randomFace());
    }
    return r;
}

const people = [
    randomGroup(),
    randomGroup(),
];

class LivePetition extends React.Component {

    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        recent_messages: PropTypes.object,
        data: PropTypes.object
    };

    state = {
        anims: function initial() {
            const v = [bounce1, bounce2, bounce3];
            for (let i = 0; i < 80; i++) {
                v.push(undefined);
            }
            return v;
        }(),
        recent_messages: { data: null, stats: {total: 0}},
        data: null,
        total: 0,
        prev_total: 0
    }

    componentDidMount() {
        // 군중 애니메이션
        const anims = this.state.anims;
        const r = [];
        for (let i = anims.length - 1; i > -1; i--) {
            r.push(anims[i]);
        }
        if (this.animating) return;
        this.animating = setInterval(() => {
            this.setState({
                anims: r
            });
        }, 3600);
    }

    componentWillReceiveProps(nextProps) {
        const { recent_messages, data } = nextProps;
        const curr_recent_messages = this.state.recent_messages.data;
        const old_total = this.state.total;
        // 새로운 데이터 플래그
        if (recent_messages.data) {
            recent_messages.data.forEach(item => {
                const exist = _.findIndex(curr_recent_messages, {id: item.id});
                item._new_flag = exist === -1;
            });

            this.setState({
                recent_messages,
                total: recent_messages.stats.total,
                prev_total: old_total
            });
        }

        // 최근 메시지 가져오기 시작
        if (data && data.data && !curr_recent_messages) {
            this.props.dispatch(fetchRecentMessages());
            this.startFetchInterval();
        }
    }

    componentWillUnmount() {
        if (this.animating) {
            clearInterval(this.animating);
        }

        if (this.fetching) {
            clearInterval(this.fetching);
        }
    }

    animating = false;
    fetching = null;

    startFetchInterval() {
        if (!this.fetching) {
            this.fetching = setInterval(() => {
                this.props.dispatch(fetchRecentMessages());
            }, 10000);
        }
    }

    getBottomDecoEl(group) {
        const anims = this.state.anims;
        const els = [];
        for (let i = 0; i < FACE_CNT; i++) {
            const j = Math.floor(Math.random() * anims.length);
            els.push(<img key={'f-' + i} className={anims[j]} src={people[group][i]} />);
        }
        return els;
    }

    getMessageItemEl(item) {
        const user = item.user;
        const who = [];
        const s = user.sex === '남성';
        let imgId = (user.id % 12) + (s ? 12 : 0) + 1;
        imgId = imgId < 10 ? '0' + imgId : imgId;
        user.district ? who.push(user.district.split(' ')[0]) : '';
        user.age ? who.push(user.age + '대') : '';
        user.job ? who.push(user.job) : '';

        const d = moment.utc(item.created).fromNow();
        return (
            <li key={'recent-message-item-' + item.id}>
                <p>{item.message}</p>
                <img src={require(`../src/i/face/${imgId}.png`)} />
                <span>{who.join(' ')} - {item.congressman.name} 의원에게</span>
                <small>({d})</small>
            </li>
        );
    }

    render() {
        const { recent_messages, total, prev_total } = this.state;
        return (
            <section className={section} style={{
                minHeight: innerHeight()
            }}>
                <h1>실시간 청원 현황</h1>
                <h4>
                    현재까지 <span>
                    <CountUp
                        start={prev_total}
                        end={total}
                        duration={2.75}
                        useEasing={true}
                        useGrouping={true}
                        separator=","
                        decimals={0}
                        decimal=","
                        prefix=""
                        suffix=""
                    />
                    </span> 명이 청원하였습니다</h4>
                <ul>
                    <FlipMove duration={750} easing="ease-out" enterAnimation="fade" leaveAnimation="fade">
                        {recent_messages.data ? recent_messages.data.map(item => this.getMessageItemEl(item)) : <span></span>}
                    </FlipMove>
                </ul>
                <div className={bottomDeco}>
                    <div>
                        {this.getBottomDecoEl(0)}
                    </div>
                    <div>
                        {this.getBottomDecoEl(1)}
                    </div>
                </div>
            </section>
        );
    }
}

export default connect(state => ({
    recent_messages: state.recent_messages,
    data: state.data
}))(LivePetition);
