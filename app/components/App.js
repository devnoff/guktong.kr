// import React from 'react';
// // import { Link } from 'react-router-dom';
// // import { footer } from '../styles/footer.scss';
// // import Routes from '../routes';

// import {} from '../styles/common.scss';
// import IntroSection from './Intro';
// import Navigation from './Navigation';
// import FirstSection from './FirstSection';
// import LivePetition from './LivePetition';
// import CongressStatus from './CongressStatus';
// // import UIs from './UIs';
// import Footer from './Footer';
// import { StickyContainer, Sticky } from 'react-sticky';
// import {ReactHeight} from 'react-height';
// import innerHeight from 'ios-inner-height';
// import { Element, Events } from 'react-scroll';

// class App extends React.Component {

//     state = {
//         introHeight: 0
//     }

//     componentWillMount() {
//         this.updateDimensions();
//     }

//     componentDidMount() {
//         window.addEventListener('resize', this.updateDimensions.bind(this));
//         window.addEventListener('scroll', this.onScrollHandler.bind(this));

//         Events.scrollEvent.register('end', (to) => {
//             this.currPos = this.menuItems.indexOf(to);
//         });
//     }

//     componentWillUnmount() {
//         window.removeEventListener('resize', this.updateDimensions);
//         Events.scrollEvent.remove('end');
//     }

//     menuItems = ['intro', 'about', 'find', 'live'];
//     currPos = 0;
//     scrollPosY = 0;

//     onIntroHeightReady(height) {
//         this.setState({
//             introHeight: height
//         });
//     }

//     updateDimensions() {
//         this.setState({width: window.innerWidth, height: innerHeight()});
//     }

//     onScrollHandler() {
//         this.scrollPosY = window.scrollY;
//     }

//     render() {
//         const { introHeight } = this.state;
//         return  (
//             <StickyContainer>
//                 <ReactHeight onHeightReady={this.onIntroHeightReady.bind(this)}>
//                     <Element name="intro"><IntroSection/></Element>
//                 </ReactHeight>
//                 {introHeight > 0 ? (
//                     <Sticky topOffset={introHeight} disableCompensation={ true }>
//                     {
//                         ({style, distanceFromTop}) => { return <Navigation style={style} topOffset={introHeight} distanceFromTop={distanceFromTop}/>;}
//                     }
//                     </Sticky>
//                 ) : undefined}
//                 <Element name="about"><FirstSection/></Element>
//                 <Element name="live"><LivePetition/></Element>
//                 <Element name="status"><CongressStatus/></Element>
//                 {/* <UIs/> */}
//                 <Footer/>
//             </StickyContainer>
//         );
//     }
// }

// export default App;

import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Entry from '../components/Entry';
import Main from '../components/Main';

export default (
    <Route>
        <Switch>
            <Route exact path="/" component={Main} />
            <Route path="/entry" component={Entry} />
        </Switch>
    </Route>
);
