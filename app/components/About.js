import React from 'react';
import { section } from '../styles/about.scss';
import innerHeight from 'ios-inner-height';
import { scroller } from 'react-scroll';

const About = () =>
    <section className={section} style={{minHeight: innerHeight()}}>
        <div>
            <img src="https://www.assembly.go.kr/resources/web/assm/images/img/img_assemblyCi_02.gif"/>
            <h4><span>소통 첫번째</span>,</h4>
            <h1>"국민 투표법 개정을 촉구합니다"</h1>
            <button onClick={()=>{
                scroller.scrollTo('find', {
                    duration: 1000,
                    delay: 100,
                    smooth: true
                });
            }}>의원찾기・청원하기</button>
            <p>소통을 원하는 국회의원을 찾아 <span style={{display: 'inline-block'}}>개헌에 대한 여러분의 의견을 전달하세요.</span></p>
        </div>
    </section>;


export default About;
