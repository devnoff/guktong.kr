import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { section, step1Begin, step2Find, step3Send, step4login, step5done, active } from '../styles/first.scss';
import innerHeight from 'ios-inner-height';
import BeginBox from './Begin';
import FindCongress from './FindCongress';
import SendPetition from './SendPetition';
import SocialLogin from './SocialLogin';
import Sent from './Sent';
import { scroller } from 'react-scroll';
import { saveTempFormData, sendUserPetition } from '../actions';

const STEP_BEGIN = 1;
const STEP_FIND  = 2;
const STEP_SEND  = 3;
const STEP_LOGIN = 4;
const STEP_DONE  = 5;

class FirstSection extends React.Component {

    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        data: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            innerHeight: window.innerHeight,
            currStep: STEP_BEGIN,
            selectedCongress: null,
            sectionHeight: null,
            sentPetition: false,
            data: null,
            show_continue_msg: false
        };
    }

    componentWillMount() {
        this.updateDimensions();
    }

    componentDidMount() {
        window.addEventListener('resize', this.updateDimensions.bind(this));
    }

    componentWillReceiveProps(nextProps) {
        const { data } = nextProps;
        const show_continue_msg = data.tmp_form_data ? true : false;
        let currStep = STEP_BEGIN;

        if (data.user && data.user.user_petitions.length > 0) {
            currStep = STEP_BEGIN;
            this.scrollToTop();
        } else if (data.show_social_login) {
            currStep = STEP_LOGIN;
            this.scrollToTop();
        } else if (data.tmp_form_data) {
            currStep = STEP_SEND;
            this.scrollToTop();
            if (data.user && data.tmp_form_data.message) {
                this.props.dispatch(sendUserPetition(data.tmp_form_data));
                return;
            }
        }

        this.setState({ data, show_continue_msg, currStep });
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }

    updateDimensions() {
        this.setState({innerHeight: innerHeight()});
    }

    childbox = []

    scrollToTop() {
        // 섹션 상단으로 스크롤
        scroller.scrollTo('about', {
            duration: 800,
            delay: 0,
            smooth: true
        });
    }

    handleNext(nextStep, congress) {
        const currStep = nextStep;
        console.log(this.childbox[nextStep]);
        this.setState({
            currStep,
            selectedCongress: congress,
            sectionHeight: this.childbox[nextStep].state.height
        });

        this.scrollToTop();
    }

    handlePrev(prevStep, congress) {
        const currStep = prevStep;
        this.setState({
            currStep,
            selectedCongress: congress,
            sectionHeight: this.childbox[prevStep].state.height
        });

        this.scrollToTop();
    }

    handleSelectedCongress(data) {
        this.handleNext(STEP_SEND, data);
    }

    handleChildHeightChange(child, height) {
        // console.log(child, height, 'child height');

        if (child === this.state.currStep) {
            this.setState({
                sectionHeight: height
            });
        }
    }

    handleClickContinue() {
        this.setState({
            show_continue_msg: false,
            currStep: STEP_SEND
        });
    }

    handleClickReset() {
        this.props.dispatch(saveTempFormData(null)).then(()=>{
            this.setState({
                show_continue_msg: false,
            });
        });
    }

    render() {
        const { currStep, selectedCongress, sectionHeight, data } = this.state;
        /* data && show_continue_msg */
        if (data) {
            return (
                <section className={section} style={{minHeight: innerHeight(), ...(sectionHeight !== null ? { height: sectionHeight} : undefined)}}>
                    <div className={[step1Begin, (currStep === STEP_BEGIN ? active : undefined)].join(' ')}>
                        <BeginBox
                            ref={ el => (this.childbox[STEP_BEGIN] = el) }
                            onNext={this.handleNext.bind(this, STEP_FIND, null)}
                            onHeightChange={this.handleChildHeightChange.bind(this, STEP_BEGIN)}
                        />
                    </div>
                    <div className={[step2Find, (currStep === STEP_FIND ? active : undefined)].join(' ')}>
                        <FindCongress
                            ref={ el => (this.childbox[STEP_FIND] = el) }
                            active={currStep === 2}
                            onSelect={this.handleSelectedCongress.bind(this)}
                            onBack={this.handlePrev.bind(this, STEP_BEGIN, null)}
                            onHeightChange={this.handleChildHeightChange.bind(this, STEP_FIND)}
                        />
                    </div>
                    <div className={[step3Send, (currStep === STEP_SEND ? active : undefined)].join(' ')}>
                        <SendPetition
                            ref={ el => (this.childbox[STEP_SEND] = el) }
                            congress={selectedCongress}
                            onBack={this.handlePrev.bind(this, STEP_BEGIN, null)}
                            onHeightChange={this.handleChildHeightChange.bind(this, STEP_SEND)}
                        />
                    </div>
                    <div className={[(currStep === STEP_LOGIN ? active : undefined), step4login].join(' ')}>
                        <SocialLogin
                            ref={ el => (this.childbox[STEP_LOGIN] = el) }
                            onHeightChange={this.handleChildHeightChange.bind(this, STEP_LOGIN)}
                        />
                    </div>
                    <div className={[step5done, (currStep === STEP_DONE ? active : undefined)].join(' ')}>
                        <Sent
                            ref={ el => (this.childbox[STEP_DONE] = el) }
                            onHeightChange={this.handleChildHeightChange.bind(this, STEP_DONE)}
                        />
                    </div>
                </section>
            );
        }
        return <section className={section} style={{minHeight: innerHeight(), ...(sectionHeight !== null ? { height: sectionHeight} : undefined)}}></section>;
    }
}

export default connect(state => ({
    data: state.data,
}))(FirstSection);
