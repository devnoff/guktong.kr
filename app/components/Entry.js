import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { section } from '../styles/intro.scss';
import innerHeight from 'ios-inner-height';
import {
    facebookLogin, naverLogin,
    fetchUserDataWithToken, fetchData
} from '../actions';

function currentUrl() {
    return window.location.href.replace(/\?.*$/, '');
}

function qs(s) {
    if (!s || s.length < 1) return {};
    const search = s.substring(1);
    return JSON.parse(
        '{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}',
        function j(key, value) {
            return key === '' ? value : decodeURIComponent(value);
        }
    );
}

class Entry extends React.Component {

    static propTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
        dispatch: PropTypes.func.isRequired,
        data: PropTypes.object
    }

    componentWillMount() {
        if (!this.props.data) {
            this.props.dispatch(fetchData());
        } else {
            this.handleRoute(this.props.data);
        }
    }

    componentWillReceiveProps(nextProps) {
        const { data } = nextProps;
        this.handleRoute(data);
    }

    handleRoute(data) {
        const hash = this.props.location.hash;
        const query = qs(this.props.location.search);
        console.log(query, 'query');
        const facebook_token = hash && hash.indexOf('access_token=') > -1
            ? hash.split('access_token=')[1].split('&')[0]
            : null;
        const naver_token = query.code || null;
        const naver_state = query.state || null;
        console.log(this.props);

        // 토큰만 존재 - 새로고침 시
        if (data.access_token && !data.user && !query.login && !facebook_token &&
            !naver_token) {
            this.props.dispatch(fetchUserDataWithToken(data.access_token))
                .then(() => {
                    this.props.history.replace('/');
                });
        } else if (!data.user || !data.user.id) {// 비로그인 시
            if (query.error && query.error_reason === 'user_denied') {
                this.props.history.replace('/'); // 홈으로 - 페이스북에서 권한 거절시
            }
            // 로그인 콜백 처리
            if (facebook_token || (naver_token && naver_state)) {
                let loginAction;
                if (facebook_token) {
                    loginAction = facebookLogin(facebook_token);
                } else if (naver_token) {
                    loginAction = naverLogin(naver_token, naver_state);
                }
                this.props.dispatch(loginAction).then(resData => {
                    const { error } = resData.result;
                    if (error) return;
                    this.props.history.replace('/');
                });
            } else if (query.login === 'naver') { // 로그인 요청 처리 - 네이버
                this.redirectToNaverOauthPage(currentUrl());
            } else if (query.login === 'facebook') { // 로그인 요청 처리 - 페이스북
                this.redirectToFacebookOauthPage(currentUrl());
            }
        } else {
            this.props.history.replace('/');
        }
    }

    redirectToNaverOauthPage(redirectUri) {
        const clientId = 'P91gQJlyH86Kpp_2LhVG';
        const apiUrl = 'https://nid.naver.com/oauth2.0/authorize?' +
            'response_type=code&client_id=' + clientId +
            '&redirect_uri=' + encodeURIComponent(redirectUri) +
            '&state=' + Math.random();
        window.location.href = apiUrl;
    }

    redirectToFacebookOauthPage(redirectUri) {
        window.location.href = 'https://www.facebook.com/dialog/oauth?' +
            'auth_type=rerequest&scope=email&' +
            'response_type=token&redirect_uri=' +
            `${encodeURIComponent(redirectUri)}&client_id=1776599675696146`;
    }

    render() {
        return (
            <section className={section} style={{ backgroundImage: `url(${require('../src/i/cubes.png')})`, minHeight: (innerHeight() * 0.6) }}>
                로그인..
            </section>
        );
    }
}

export default connect(state => ({
    data: state.data,
}))(Entry);
