import PropTypes from 'prop-types';
import React from 'react';
import { nav, mobileHeader, mobileClose, navActive, liActive, ulActive, ulInactive, navInactive, logo } from '../styles/navigation.scss';
import { onMobileMenuDisplaying } from '../styles/common.scss';
import { ScrollLink, animateScroll as scroll } from 'react-scroll';


class Link extends React.Component {
    render() {
        return (
              <li {...this.props}>
                {this.props.children}
              </li>
        );
    }
}

Link.propTypes = {
    children: PropTypes.element,
};

Link = ScrollLink(Link);

class Navigation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showMenu: undefined,
            distanceFromTop: props.distanceFromTop,
            topOffset: props.topOffset
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.hasOwnProperty('distanceFromTop')) {
            this.setState({
                distanceFromTop: nextProps.distanceFromTop,
                topOffset: nextProps.topOffset
            });
        }
    }

    openMenu() {
        if (!this.state.showMenu) {
            this.setState({showMenu: true});
            document.body.classList.add(onMobileMenuDisplaying);
        }
    }

    closeMenu() {
        if (this.state.showMenu) {
            this.setState({showMenu: false});
            document.body.classList.remove(onMobileMenuDisplaying);
        }
    }

    onPressMenu() {
        this.openMenu();
    }

    onPressClose() {
        this.closeMenu();
    }

    handleSetActive() {
        this.closeMenu();
    }

    render() {
        const { showMenu, distanceFromTop, topOffset } = this.state;
        let navClass = [nav];
        if (showMenu !== undefined) {
            navClass.push(showMenu ? navActive : navInactive);
        }
        navClass = navClass.join(' ');

        let navPosY = 0;
        if (topOffset + distanceFromTop < 0 ) {
            navPosY = 0;
        } else {
            navPosY = topOffset + distanceFromTop;
        }

        return  (<nav style={!isNaN(navPosY) ? {top: navPosY} : undefined}>
            <div className={mobileHeader} onClick={this.onPressMenu.bind(this)}>메뉴</div>
            <div className={navClass} onClick={showMenu ? this.onPressClose.bind(this) : undefined}>
                <ul className={showMenu ? ulActive : ulInactive} style={{ponterEvents: showMenu ? 'auto' : 'none'}}>
                    <li onClick={()=>{scroll.scrollToTop();}} className={logo}>국회소통<span></span><span>GUKTONG.KR</span><span className={mobileClose} onClick={this.onPressClose.bind(this)}>✕</span></li>
                    <Link onClick={this.handleSetActive.bind(this)} activeClass={liActive} to="about" spy={true} smooth={true}><span>청원하기</span></Link>
                    <Link onClick={this.handleSetActive.bind(this)} activeClass={liActive} to="live" spy={true} smooth={true}><span>청원 현황</span></Link>
                    <Link onClick={this.handleSetActive.bind(this)} activeClass={liActive} to="status" spy={true} smooth={true}><span>응답현황</span></Link>
                </ul>
            </div>
        </nav>);
    }
}

Navigation.propTypes = {
    style: PropTypes.object,
    distanceFromTop: PropTypes.number,
    topOffset: PropTypes.number
};

export default Navigation;
