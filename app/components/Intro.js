import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { section } from '../styles/intro.scss';
import innerHeight from 'ios-inner-height';
// import { fetchData } from '../actions';

class Intro extends React.Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        history: PropTypes.object.isRequired,
        data: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            introHeight: 0,
            user: props.data ? props.data.user : null
        };
    }

    componentWillMount() {
        // this.props.dispatch(fetchData());
    }

    componentWillReceiveProps(nextProps) {
        const { data } = nextProps;
        this.setState({
            user: data.user
        });
    }

    render() {
        return (
            <section
                className={section}
                style={{
                    backgroundImage: `url(${require('../src/i/cubes.png')})`,
                    minHeight: innerHeight() * 0.6
                }}
            >
                <div>
                    <h1>
                        <img src={require('../src/i/logo_draft.png')} />
                    </h1>
                    <p>
                        개헌에 대한 <strong>여러분</strong>의 의견을{' '}
                        <strong>국회의원</strong>에게 전하고{' '}
                        <span>
                            시민과의 <strong>소통</strong>을 요청합니다.
                        </span>
                    </p>
                    <small>
                        <a href="http://www.yonhapnews.co.kr/bulletin/2018/04/04/0200000000AKR20180404146251004.HTML">
                        헌재 헌법불합치 결정한 국민투표법 조항, <span>3년째 효력상실 상태 [연합뉴스]</span>
                        </a>
                        <a href="http://www.hani.co.kr/arti/politics/bluehouse/841805.html">
                        문 대통령 “국회, 헌법개정안 <span>심의조차 안 해…국민들께 유감” [한겨례]</span>
                        </a>
                    </small>
                </div>
            </section>
        );
    }
}

export default connect(state => ({
    data: state.data
}))(Intro);
