import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import _ from 'lodash';
import {
    section,
    card,
    detailBox,
    active,
    lazyActive,
    imgBox,
    content,
    buttonBox,
    form,
    personal,
    formTop,
    formBottom,
    formButton,
    btnActive,
    lazyFadeIn,
    reactModalPortal,
    modalButton
} from '../styles/sendPetition.scss';
import theme from '../styles/autosuggest.scss';
import innerHeight from 'ios-inner-height';
import Autosuggest from 'react-autosuggest';
import { ReactHeight } from 'react-height';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import MaterialIcon from 'material-icons-react';
import FaTwitterSquare from 'react-icons/lib/fa/twitter-square';
import FaFacebookSquare from 'react-icons/lib/fa/facebook-square';
import FaGlobe from 'react-icons/lib/fa/globe';
import MdClose from 'react-icons/lib/md/close';
// import ScrollLock from 'react-scrolllock';
import Modal from 'react-modal';
import 'whatwg-fetch';
import { saveTempFormData, showSocialLogin, sendUserPetition, fetchLawmakers } from '../actions';

const ageOptions = [
    { value: 20, label: '20대' },
    { value: 30, label: '30대' },
    { value: 40, label: '40대' },
    { value: 50, label: '50대' },
    { value: 60, label: '60대 이상' }
];

const jobOptions = [
    { value: '자영업', label: '자영업' },
    { value: '직장인', label: '직장인' },
    { value: '전문직', label: '전문직' },
    { value: '자유직', label: '자유직' },
    { value: '사업가', label: '사업가' },
    { value: '가정주부', label: '가정주부' },
    { value: '학생', label: '학생' },
    { value: '퇴직자', label: '퇴직자' },
    { value: '공무원', label: '공무원' },
    { value: '기타', label: '기타' }
];

const sexOptions = [
    { value: '여성', label: '여성' },
    { value: '남성', label: '남성' }
];

// const partyOptions = [
//     {value: '더불어민주당', label: '더불어민주당'},
//     {value: '자유한국당', label: '자유한국당'},
//     {value: '바른미래당', label: '바른미래당'},
//     {value: '민주평화당', label: '민주평화당'},
//     {value: '정의당', label: '정의당'},
//     {value: '민중당', label: '민중당'},
//     {value: '대한애국당', label: '대한애국당'}
// ];

const getSuggestionValue = area => area.district;
const renderSuggestion = area => <span>{area.district}</span>;
const shouldRenderSuggestions = value => value.length > 1;

class SendPetition extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showPreview: false,
            showDetail: false,
            congress: null,
            allAreaOptions: null,
            areaOptions: [],
            form_data: null,
            message: '',
            personal_info: {
                age: null,
                sex: null,
                district: '',
                job: null
            },
            data: null
        };
    }

    componentDidMount() {
        fetch('/districts.json')
            .then(res => res.json())
            .then(json => {
                const allAreaOptions = json;
                const nextState = { allAreaOptions };
                nextState.personal_info = this.state.personal_info;
                this.setState(nextState);
            });

        this.props.dispatch(fetchLawmakers());
    }

    componentWillReceiveProps(nextProps) {
        const { data, lawmakersData } = nextProps;
        let congress = nextProps.congress;
        const newState = { data, lawmakersData };
        if (congress) {
            if (lawmakersData && congress) {
                congress = lawmakersData[congress.id];
            }
            newState.congress = congress;
        }

        const form_data = data ? data.tmp_form_data : null;
        if (form_data) {
            newState.message = form_data.message || '';
            newState.congress = lawmakersData
                ? lawmakersData[form_data.congressman_id]
                : { id: form_data.congressman_id };
            newState.personal_info = {
                age: form_data.age,
                sex: form_data.sex,
                district: form_data.district || '',
                job: form_data.job
            };
        }

        this.setState(newState);
    }

    handleClickSocialLogin() {
        // Validation
        if (!this.validate()) {
            return;
        }

        const { message, congress } = this.state;
        const formData = {
            petition_id: this.props.data.data.id,
            congressman_id: congress.id,
            message,
            ...this.state.personal_info
        };
        this.props.dispatch(saveTempFormData(formData)).then(() => {
            // 소셜 로그인
            this.props.dispatch(showSocialLogin(true));
        });
    }

    validate() {
        const { message, personal_info } = this.state;
        const { age, sex, job, district } = personal_info;
        // Validation
        if (message.length < 1) {
            alert('메세지를 입력하세요.');
            this.messageInput.focus();
            return false;
        }

        if (!age) {
            alert('연령대를 선택하세요.');
            this.ageSelect.focus();
            return false;
        }

        if (!sex) {
            alert('성별을 입력하세요.');
            this.sexSelect.focus();
            return false;
        }

        if (!job) {
            alert('직업을 입력하세요.');
            this.jobSelect.focus();
            return false;
        }

        if (!district) {
            alert('사는곳을 선택하세요.');
            return false;
        }

        return true;
    }

    handleClickSend() {
        // Validation
        if (!this.validate()) {
            return;
        }

        const { message, congress, personal_info } = this.state;

        const formData = {
            petition_id: this.props.data.data.id,
            congressman_id: congress.id,
            message,
            ...personal_info
        };
        this.props.dispatch(sendUserPetition(formData)).then(() => {

        });
    }

    handleClickBack() {
        this.props.dispatch(saveTempFormData(null)).then(() => {
            this.setState({ congress: null, showDetail: false });
            this.props.onBack();
        });
    }

    handlePersonalInfoChange(k, v) {
        const { personal_info } = this.state;
        personal_info[k] = v.value || v;
        this.setState({ personal_info });
    }

    handleAreaChange(e, { newValue }) {
        const { personal_info } = this.state;
        personal_info.district = newValue;

        // https://trello.com/c/D5y0m89O/72-3
        // 지역 검색 후, 하나를 선택했을 때 마지막 단어 '동'이 붙는 문제 방지
        if (e.keyCode === 229 || e.keyCode === 40) {
            setTimeout(() => {
                this.setState({ personal_info });
            }, 0);
        } else {
            this.setState({ personal_info });
        }

        e.preventDefault();
    }

    handleMessageChange(e) {
        this.setState({
            message: e.target.value
        });
    }

    handleClickPreview() {
        this.setState({
            showPreview: true
        });
    }

    handleClickClosePreview() {
        this.setState({
            showPreview: false
        });
    }

    toggleDetail() {
        const { showDetail } = this.state;
        this.setState({
            showDetail: !showDetail
        });
    }

    onSuggestionsFetchRequested({ value }) {
        const { allAreaOptions } = this.state;
        const areaOptions = [];
        for (const i in allAreaOptions) {
            if (
                !value ||
                allAreaOptions[i].district.indexOf(value) > -1 ||
                allAreaOptions[i].sublocality.indexOf(value) > -1
            ) {
                areaOptions.push(allAreaOptions[i]);
            }
        }
        this.setState({ areaOptions });
    }

    onSuggestionsClearRequested() {
        this.setState({ areaOptions: [] });
    }

    onHeightReady(height) {
        this.props.onHeightChange(height);
        this.setState({ height: height });
    }

    getCongressCardEl(data) {
        const { showDetail } = this.state;
        if (!data || !data.name) return undefined;
        return (
            <div className={card}>
                <div>
                    <div className={imgBox}>
                        <img
                            src={
                                'http://parkgeunhack.com/img/' +
                                data.id +
                                '.jpg'
                            }
                        />
                    </div>
                    <div className={content}>
                        <h4>
                            <span>To. </span>
                            {data.name} 의원
                        </h4>
                        <p>
                            <span>{data.party.name}</span> - {data.district}
                        </p>
                    </div>
                    <div className={buttonBox}>
                        {showDetail ? (
                            <button onClick={this.toggleDetail.bind(this)}>
                                접기
                            </button>
                        ) : (
                            <button onClick={this.toggleDetail.bind(this)}>
                                의원정보 보기{' '}
                                <MaterialIcon
                                    size={20}
                                    icon="search"
                                    color="#ddd"
                                />
                            </button>
                        )}
                    </div>
                </div>
                <div
                    className={[
                        detailBox,
                        showDetail ? active : undefined
                    ].join(' ')}
                >
                    <div
                        className={showDetail ? lazyActive : undefined}
                        style={{ position: 'absolute' }}
                    >
                        <div>
                            {data.website ? (
                                <div>
                                    <a target="blank_" href={data.website}>
                                        <FaGlobe />
                                        <span>홈페이지</span>
                                    </a>
                                </div>
                            ) : (
                                undefined
                            )}
                            {data.twitter ? (
                                <div>
                                    <a target="blank_" href={data.twitter}>
                                        <FaTwitterSquare />
                                        <span>트위터</span>
                                    </a>
                                </div>
                            ) : (
                                undefined
                            )}
                            {data.facebook ? (
                                <div>
                                    <a target="blank_" href={data.facebook}>
                                        <FaFacebookSquare />
                                        <span>페이스북</span>
                                    </a>
                                </div>
                            ) : (
                                undefined
                            )}
                        </div>
                        <div>
                            {data.phone ? (
                                <div>
                                    <MaterialIcon icon="local_phone" />
                                    {data.phone}
                                </div>
                            ) : (
                                undefined
                            )}
                            {data.fax ? (
                                <div>
                                    <MaterialIcon icon="local_printshop" />
                                    {data.fax}
                                </div>
                            ) : (
                                undefined
                            )}
                            {data.office ? (
                                <div>
                                    <MaterialIcon icon="domain" />
                                    {data.office}
                                </div>
                            ) : (
                                undefined
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const {
            congress,
            personal_info,
            areaOptions,
            showPreview,
            message,
            data
        } = this.state;

        return (
            <ReactHeight
                onHeightReady={this.onHeightReady.bind(this)}
                className={section}
                style={{ minHeight: innerHeight() }}
            >
                <h1>청원 보내기</h1>
                {this.getCongressCardEl(congress)}
                <div className={form}>
                    <div>
                        {/* <small>의원님께 묻습니다</small> */}
                        <h3>
                            "국민투표법 개정을 촉구합니다"
                        </h3>
                        <h2 style={{
                            maxWidth: '70%',
                            backgroundColor: 'white',
                            color: '#333',
                            margin: '0 auto 2rem auto',
                            fontSize: '1rem',
                            border: '1px solid #eee',
                            boxShadow: '0 0 3px #ddd',
                            lineHeight: '1.5',
                            wordBreak: 'keep-all',
                            padding: '10%'}}>
                            <small style={{display: 'inline-block',
                            fontSize: '.8rem',
                            color: '#bbb',
                            borderBottom: 'none',
                            fontFamily: 'NanumSquare',
                            fontWeight: 'normal',
                            paddingBottom: '1.3rem'}}>그리고 묻습니다.</small><br/>
                        "{data && data.data.question ? data.data.question : ''}"
                        </h2>
                        <input
                            ref={el => (this.messageInput = el)}
                            onChange={this.handleMessageChange.bind(this)}
                            value={message}
                            name="message"
                            type="text"
                            placeholder="한줄 메세지를 남겨주세요."
                        />
                        <div className={personal}>
                            <div className={formTop}>
                                <Select
                                    name="age"
                                    ref={el => (this.ageSelect = el)}
                                    options={ageOptions}
                                    searchable={false}
                                    clearable={false}
                                    value={personal_info.age}
                                    placeholder="연령"
                                    onChange={this.handlePersonalInfoChange.bind(
                                        this,
                                        'age'
                                    )}
                                />
                                <Select
                                    name="sex"
                                    ref={el => (this.sexSelect = el)}
                                    options={sexOptions}
                                    searchable={false}
                                    clearable={false}
                                    value={personal_info.sex}
                                    placeholder="성별"
                                    onChange={this.handlePersonalInfoChange.bind(
                                        this,
                                        'sex'
                                    )}
                                />
                            </div>
                            <div className={formBottom}>
                                {/* <Select name="party" options={ partyOptions }
                                        searchable={ false }
                                        clearable={ false }
                                        value={ personal_info.party }
                                        placeholder="지지정당"
                                        onChange={ this.handlePersonalInfoChange.bind(this,
                                            'party') } /> */}
                                <Select
                                    name="job"
                                    ref={el => (this.jobSelect = el)}
                                    options={jobOptions}
                                    searchable={false}
                                    clearable={false}
                                    value={personal_info.job}
                                    placeholder="직업"
                                    onChange={this.handlePersonalInfoChange.bind(
                                        this,
                                        'job'
                                    )}
                                />
                                <Autosuggest
                                    theme={theme}
                                    ref={el => (this.districtSelect = el)}
                                    suggestions={areaOptions}
                                    shouldRenderSuggestions={
                                        shouldRenderSuggestions
                                    }
                                    onSuggestionsFetchRequested={this.onSuggestionsFetchRequested.bind(
                                        this
                                    )}
                                    onSuggestionsClearRequested={this.onSuggestionsClearRequested.bind(
                                        this
                                    )}
                                    getSuggestionValue={getSuggestionValue}
                                    renderSuggestion={renderSuggestion}
                                    focusFirstSuggestion={true}
                                    inputProps={{
                                        placeholder: '사는 곳을 입력하세요',
                                        value: personal_info.district,
                                        onChange: this.handleAreaChange.bind(
                                            this
                                        )
                                    }}
                                />
                            </div>
                        </div>
                        <div>
                            <a
                                style={{
                                    fontSize: '.7rem',
                                    textDecoration: 'underline',
                                    marginTop: '.5rem',
                                    color: '#aaa',
                                    cursor: 'pointer'
                                }}
                                onClick={this.handleClickPreview.bind(this)}
                            >
                                국회의원에게 이렇게 보여집니다.(미리보기)
                            </a>
                        </div>
                    </div>
                </div>
                <div className={formButton}>
                    {data && data.user ? (
                        <button
                            className={btnActive}
                            onClick={this.handleClickSend.bind(this)}
                        >
                            <MaterialIcon icon="send"/>
                            청원 발송
                        </button>
                    ) : (
                        <button
                            className={btnActive}
                            onClick={this.handleClickSocialLogin.bind(this)}
                        >
                            <FaFacebookSquare color="rgba(255,255,255,0.7)" size={18}/>
                            <span style={{width: '16px',
                                        height: '16px',
                                        display: 'flex',
                                        backgroundColor: 'rgba(255,255,255,0.7)',
                                        justifyItems: 'center',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        borderRadius: '2px',
                                        marginRight: '3px',
                                        marginLeft: '3px',
                                        }}>
                                <img style={{width: '10px', height: '10px'}} src={require('../src/i/Naver_logo_initial.svg')}/>
                            </span>
                            소셜 로그인 후 보내기
                        </button>
                    )}
                    <a onClick={this.handleClickBack.bind(this)}>
                        처음부터 다시하기
                    </a>
                </div>
                <Modal
                    ariaHideApp={false}
                    isOpen={showPreview}
                    portalClassName={reactModalPortal}
                    style={{
                        overlay: {
                            top: '0',
                            left: '0',
                            right: '0',
                            bottom: '0',
                            backgroundColor: 'rgba(0,0,0,0.5)',
                            overflowY: 'scroll'
                        },
                        content: {
                            top: '0',
                            left: '0',
                            right: '0',
                            bottom: '0',
                            position: 'relative',
                            border: 'none',
                            background: 'none',
                            textAlign: 'center'
                        }
                    }}
                >
                    <img
                        onClick={this.handleClickClosePreview.bind(this)}
                        className={showPreview ? lazyFadeIn : undefined}
                        src={require('../src/i/mail_screen_3.jpg')}
                    />
                    <div className={modalButton}>
                        <button
                            style={{ verticalAlign: 'middle' }}
                            onClick={this.handleClickClosePreview.bind(this)}
                        >
                            <MdClose size={20} />
                            <span>닫기</span>
                        </button>
                    </div>
                </Modal>
            </ReactHeight>
        );
    }
}

SendPetition.propTypes = {
    congress: PropTypes.object,
    onBack: PropTypes.func.isRequired,
    onHeightChange: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired,
    data: PropTypes.object
};

export default connect(state => ({
    data: state.data,
    lawmakersData: state.lawmakers
}))(SendPetition);
