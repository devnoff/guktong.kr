import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import '../styles/common.scss';
import IntroSection from './Intro';
import Navigation from './Navigation';
import FirstSection from './FirstSection';
import LivePetition from './LivePetition';
import CongressStatus from './CongressStatus';
// import UIs from './UIs';
import Footer from './Footer';
import { StickyContainer, Sticky } from 'react-sticky';
import { ReactHeight } from 'react-height';
import innerHeight from 'ios-inner-height';
import { Element, Events } from 'react-scroll';
import { fetchUserDataWithToken, fetchData } from '../actions';

class Main extends React.Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        history: PropTypes.object.isRequired,
        data: PropTypes.object
    };

    state = {
        introHeight: 0
    };

    componentWillMount() {
        this.updateDimensions();
        this.props.dispatch(fetchData());
    }

    componentDidMount() {
        window.addEventListener('resize', this.updateDimensions.bind(this));
        window.addEventListener('scroll', this.onScrollHandler.bind(this));

        Events.scrollEvent.register('end', to => {
            this.currPos = this.menuItems.indexOf(to);
        });
    }

    componentWillReceiveProps(nextProps) {
        const { data } = nextProps;
        if (data && data.access_token && !data.user) {
            this.props.dispatch(fetchUserDataWithToken(data.access_token));
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
        Events.scrollEvent.remove('end');
    }

    menuItems = ['intro', 'about', 'find', 'live'];
    currPos = 0;
    scrollPosY = 0;

    onIntroHeightReady(height) {
        this.setState({
            introHeight: height
        });
    }

    updateDimensions() {
        this.setState({ width: window.innerWidth, height: innerHeight() });
    }

    onScrollHandler() {
        this.scrollPosY = window.scrollY;
    }

    render() {
        const { introHeight } = this.state;
        return (
            <StickyContainer>
                <ReactHeight onHeightReady={this.onIntroHeightReady.bind(this)}>
                    <Element name="intro">
                        <IntroSection history={this.props.history} />
                    </Element>
                </ReactHeight>
                {introHeight > 0 ? (
                    <Sticky topOffset={introHeight} disableCompensation={true}>
                        {({ style, distanceFromTop }) => {
                            return (
                                <Navigation
                                    style={style}
                                    topOffset={introHeight}
                                    distanceFromTop={distanceFromTop}
                                />
                            );
                        }}
                    </Sticky>
                ) : (
                    undefined
                )}
                <Element name="about">
                    <FirstSection />
                </Element>
                <Element name="live">
                    <LivePetition />
                </Element>
                <Element name="status">
                    <CongressStatus />
                </Element>
                {/* <UIs/> */}
                <Footer />
            </StickyContainer>
        );
    }
}

export default connect(state => ({
    data: state.data
}))(Main);
