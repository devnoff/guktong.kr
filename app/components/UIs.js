import React from 'react';
import { section, logo, mailcover, fbcover } from '../styles/uis.scss';
import innerHeight from 'ios-inner-height';

class CongressStatus extends React.Component {

    render() {
        return (
            <div className={section} style={{minHeight: innerHeight()}}>
                {/* <h1>응답 현황</h1> */}
                <div>
                    <h1 className={logo}>
                    <span style={{fontSize: '6rem'}}>국</span><span>회</span><span>소</span><span style={{fontSize: '5.5rem'}}>통</span>
                    </h1>
                    <span>sotong.co</span>
                </div>
                <div className={mailcover} style={{background: `url(${require('../src/i/the-national-assembly.jpg')})`}}>
                    <div>"6월 개헌 및 이를 위한 국민투표법의 신속한 처리에 찬성하시나요?"</div>
                </div>
                <div className={fbcover} style={{background: `url(${require('../src/i/the-national-assembly.jpg')})`}}>
                    국회 소통
                </div>
            </div>
        );
    }
}

export default CongressStatus;
