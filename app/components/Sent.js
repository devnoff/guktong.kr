import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { section } from '../styles/sent.scss';
import { ReactHeight } from 'react-height';
import moment from 'moment';

class Sent extends React.Component {

    static propTypes = {
        onHeightChange: PropTypes.func.isRequired,
        data: PropTypes.object,
        lawmakers: PropTypes.object
    };

    state = {
        data: null,
        lawmakers: null
    }

    componentWillReceiveProps(nextProps) {
        this.setState(nextProps);
        const newState = {};
        if (nextProps.data) newState.data = nextProps.data;
        if (nextProps.lawmakers) newState.lawmakers = nextProps.lawmakers;
        this.setState(newState);
    }

    onHeightReady(height) {
        this.props.onHeightChange(height);
        this.setState({height: height});
    }

    render() {
        const { data, lawmakers } = this.state;
        if (data && data.user && lawmakers) {
            const user_petition = data.user.user_petitions[0];
            if (user_petition) {
                const date_sent = moment.utc(user_petition.created).format('YYYY년 MM월 DD일 H시 mm분');
                const congressman = lawmakers[user_petition.id];
                return (
                    <ReactHeight onHeightReady={this.onHeightReady.bind(this)} className={section}>
                        <div>
                            <h3>청원 발송 완료</h3>
                            <div>{date_sent}에 {congressman.name}의원에게 청원을 보냈습니다.</div>
                        </div>
                    </ReactHeight>
                );
            }
        }

        return <div className={section}></div>;
    }
}

export default connect(state => ({
    data: state.data,
    lawmakers: state.lawmakers
}))(Sent);
