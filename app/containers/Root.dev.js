import PropTypes from 'prop-types';
import React from 'react';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'react-router-redux';

import App from '../components/App';
import DevTools from './DevTools';

export default function Root({store, history}) {
    console.log(store.getState());

    // 상태가 바뀔때마다 기록합니다.
    // store.subscribe(() =>
    //     console.log(store.getState())
    // );
    return (
        <Provider store={store}>
            <div>
                <ConnectedRouter history={history}>
                    {App}
                </ConnectedRouter>
                <DevTools />
            </div>
        </Provider>
    );
}

Root.propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
};
