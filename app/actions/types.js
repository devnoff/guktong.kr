export const FILTER = 'FILTER';


/* User */
export const SIGN_IN_REQUEST = 'SIGN_IN_REQUEST'; // [POST /auth/login]
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS'; //
export const SIGN_IN_FAILURE = 'SIGN_IN_FAILURE'; //

/* Congress */
export const SEARCH_CONGRESS = 'SEARCH_CONGRESS'; // [LOCAL]
export const FOUND_CONGRESS = 'FOUND_CONGRESS'; //

/* Petition */
export const PETITION_REQUEST = 'PETITION_REQUEST'; // [GET /petitions/ongoing]
export const PETITION_SUCCESS = 'PETITION_SUCCESS'; //
export const PETITION_FAILURE = 'PETITION_FAILURE'; //

/* Survey */
export const FETCH_SURVEYS = 'FETCH_SURVEYS'; // (by petition_id)

/* Question */
export const FETCH_QUESTION = 'FETCH_QUESTION'; // [LOCAL](by id)
export const FETCH_QUESTIONS = 'FETCH_QUESTIONS'; // [LOCAL](by survey_id)

/* User Survey */
export const SET_USER_SURVEY = 'SET_USER_SURVEY'; //
export const ADD_ANSWER_REQUEST = 'ADD_ANSWER_REQUEST'; // [POST /users/:id/survey/:survey_id/answers/:question_id](question_id / [answers])
export const ADD_ANSWER_SUCCESS = 'ADD_ANSWER_SUCCESS'; //
export const ADD_ANSWER_FAILURE = 'ADD_ANSWER_FAILURE'; //

/* User Petition */
export const SEND_PETITION_REQUEST = 'SEND_PETITION_REQUEST'; // [POST /users/:id/petition]
export const SEND_PETITION_SUCCESS = 'SEND_PETITION_SUCCESS'; //
export const SEND_PETITION_FAILURE = 'SEND_PETITION_FAILURE'; //
