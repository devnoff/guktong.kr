import {
    USER_PETITION_SENT
} from '../constants';

const initialState = null;
const actionsMap = {
    [USER_PETITION_SENT]: (state, action) => action.result,
};
export default function user_petition(state = initialState, action) {
    const fn = actionsMap[action.type];
    if (!fn) return state;
    return fn(state, action);
}
