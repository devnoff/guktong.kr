import { RECENT_MESSAGES_RECEIVED } from '../constants';

const initialState = { data: null, stats: {total: 0}};
const actionsMap = {
    [RECENT_MESSAGES_RECEIVED]: (state, action) => action.result,
};
export default function recent_messages(state = initialState, action) {
    const fn = actionsMap[action.type];
    if (!fn) return state;
    return fn(state, action);
}
