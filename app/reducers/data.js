import Cookies from 'js-cookie';
import { DATA_RECEIVED, DATA_UPDATED } from '../constants';

const init_tmp_form_data = Cookies.get('tmp_form_data');
const initialState = {
    access_token: Cookies.get('access_token') || null,
    tmp_form_data: init_tmp_form_data ? JSON.parse(init_tmp_form_data) : null,
    show_social_login: false,
    data: null,
    user: null
};
const actionsMap = {
    [DATA_RECEIVED]: (state, action) => action.result,
    [DATA_UPDATED]: (state, action) => action.result,
};
export default function data(state = initialState, action) {
    const fn = actionsMap[action.type];
    if (!fn) return state;
    return fn(state, action);
}
