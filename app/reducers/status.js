import { CONGRESSMAN_STATUS_RECEIVED } from '../constants';

const initialState = null;
const actionsMap = {
    [CONGRESSMAN_STATUS_RECEIVED]: (state, action) => action.result,
};
export default function status(state = initialState, action) {
    const fn = actionsMap[action.type];
    if (!fn) return state;
    return fn(state, action);
}
