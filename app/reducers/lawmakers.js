import { RECEIVED_LAWMAKERS } from '../constants';

const initialState = null;
const actionsMap = {
    [RECEIVED_LAWMAKERS]: (state, action) => action.result,
};
export default function lawmakers(state = initialState, action) {
    const fn = actionsMap[action.type];
    if (!fn) return state;
    return fn(state, action);
}
