import { RESPONDS_RECEIVED } from '../constants';

const initialState = null;
const actionsMap = {
    [RESPONDS_RECEIVED]: (state, action) => action.result,
};
export default function responds(state = initialState, action) {
    const fn = actionsMap[action.type];
    if (!fn) return state;
    return fn(state, action);
}
