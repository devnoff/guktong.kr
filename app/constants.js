export const DATA_RECEIVED = 'DATA_RECEIVED';
export const DATA_UPDATED = 'DATA_UPDATED';
export const USER_PETITION_SENT = 'USER_PETITION_SENT';
export const RECENT_MESSAGES_RECEIVED = 'RECENT_MESSAGES_RECEIVED';
export const CONGRESSMAN_STATUS_RECEIVED = 'CONGRESSMAN_STATUS_RECEIVED';
export const RESPONDS_RECEIVED = 'RESPONDS_RECEIVED';
export const SAVE_FORM_DATA = 'SAVE_FORM_DATA';
export const LOAD_FORM_DATA = 'LOAD_FORM_DATA';
export const SHOW_SOCIAL_LOGIN = 'SHOW_SOCIAL_LOGIN';
export const RECEIVED_LAWMAKERS = 'RECEIVED_LAWMAKERS';
