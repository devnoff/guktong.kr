'use strict';

var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
    devtool: 'eval-source-map',
    entry: [
        'babel-polyfill',
        'webpack-dev-server/client?http://localhost:3000',
        'webpack/hot/only-dev-server',
        'react-hot-loader/patch',
        path.join(__dirname, 'app/index.js')
    ],
    output: {
        path: path.join(__dirname, '/dist/'),
        filename: '[name].js',
        publicPath: '/'
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'app/index.tpl.html',
            inject: 'body',
            filename: 'index.html'
        }),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development')
        }),
        new CopyWebpackPlugin([
            {
                from: 'app/data/lawmakers.json',
                to: './',
                toType: 'dir'
            },
            {
                from: 'app/data/lawmakers_index.json',
                to: './',
                toType: 'dir'
            },
            {
                from: 'app/data/districts.json',
                to: './',
                toType: 'dir'
            },
        ], { force: true })
    ],
    eslint: {
        configFile: '.eslintrc',
        failOnWarning: false,
        failOnError: false
    },
    module: {
        preLoaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'eslint'
            }
        ],
        loaders: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'babel'
            },
            {
                test: /\.json?$/,
                loader: 'json',
                exclude: [
                    path.resolve(__dirname, '/app/data/lawmakers.json'),
                    path.resolve(__dirname, '/app/data/lawmakers_index.json'),
                    path.resolve(__dirname, '/app/data/districts.json'),
                ],
            },
            {
                test: /\.css$/,
                include: [
                    /node_modules/,
                    path.resolve(__dirname, '/app/styles/autosuggest.scss'),
                ],
                loader: 'style!css!postcss',
            },
            {
                test: /\.scss$|\.css$/,
                exclude: [
                    /node_modules/,
                    path.resolve(__dirname, '/app/styles/autosuggest.scss'),
                ],
                loader: 'style!css?modules&localIdentName=[name]---[local]---[hash:base64:5]!sass'
            },
            { test: /\.woff(2)?(\?[a-z0-9#=&.]+)?$/, loader: 'url?limit=10000&mimetype=application/font-woff' },
            { test: /\.(ttf|eot|svg|png|jpg)(\?[a-z0-9#=&.]+)?$/, loader: 'file' }
        ]
    }
};
