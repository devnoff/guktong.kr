export {
    user: {
        id: null,
        fb_id: null,
        naver_id: null,
        age: null,
        gender: null,
        area: null,
        job: null,
        party: null
        created: null,
        surveys: [
            {
                id: 1,
                answers: [
                    {
                        id: 1,
                        question_id: 1,
                        selected_answers: [ 1 ]
                    },
                    {
                        id: 2,
                        question_id: 2, 
                        selected_answers: [ 3 ]
                    }
                ]
            }
        ],
        petitions: [
            {
                id: 1,
                selected_congress_id: null,
                message: null,
                sent: null
            }
        ]
    },
    congress_memebers: [], // load remotely
    current_petition: 1,
    petitions: [
        {
            id: 1,
            title: "소통 첫번째,",
            content: "국민투표법 개정을 촉구합니다",
            references: [
                // 국민 투표법 관련 기사
                {
                    id: 1,
                    link: "http://~",
                    title: ""
                }
            ],
        },
        {
            id: 2,
            title: "소통 두번째, 국회의원께 묻습니다",
            content: "한국당과 정부의 개헌안 중 선호하는 것은 무엇입니까?"
        },
    ],
    surveys: [
        {
            id: 1,
            petition_id: 2,
            title: "개헌 2018 국회소통 릴레이청원 첫번째",
            questions: [ 1, 2 ]
        }
    ],
    questions: [
        {
            id: 1,
            survey_id: 1,
            title: "한국당과 정부의 개헌안 중 지지하는 것은?",
            short_title: "선호 개헌안",
            answers: [
                0,
                1,
                3
            ],
            type: "CHOICE" // or SUBJECTIVE
        },
        {
            id: 2,
            survey_id: 1,
            title: "적절한 국민투표 시기는?",
            short_title: "선호 국민투표시기",
            answers: [
                3,
                4,
                2
            ],
            type: "CHOICE" // or SUBJECTIVE
        }
    ],
    answers: [
        { id: 0, title: "한국당안"},
        { id: 1, title: "정부안"},
        { id: 2, title: "기타"},
        { id: 3, title: "지방선거와 동시에"},
        { id: 4, title: "9월 이후",
    ]
}